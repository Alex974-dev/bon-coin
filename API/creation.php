<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../Config/database.php';
    include_once '../Class/Produit.php';

    $database = new Database();
    $db = $database->getConnection(); 

    $item = new Produit($db);

    $data = json_decode(file_get_contents("php://input"));

    // var_dump($data);

    $item->marque = $data->marque;
    $item->model = $data->model;
    $item->prix = $data->prix;
    
    if($item->ajoutProduit()){
        echo 'Produit ajouter avec succès !';
    } else{
        echo 'Produit non créer !';
    }
?>