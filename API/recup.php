<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    
    include_once '../Config/Database.php';
    include_once '../Class/Produit.php';

    $database = new Database();
    $db = $database->getConnection();

    $items = new Produit($db);

    $stmt = $items->getProduitAll();
    $itemCount = $stmt->rowCount();
    


    echo json_encode($itemCount);

    if($itemCount > 0){
        
        $produit = array();
        $produit["body"] = array();
        $produit["itemCount"] = $itemCount;

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $produit_id,
                "marque" => $marque,
                "model" => $model,
                "prix" => $prix,
            );

            array_push($produit["body"], $e);
        }
        echo json_encode($produit);
    }

    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "Erreur")
        );
    }
?>