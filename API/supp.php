<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once '../Config/database.php';
    include_once '../Class/Produit.php';
    
    $database = new Database();
    $db = $database->getConnection();
    
    $item = new Produit($db);
    
    $data = json_decode(file_get_contents("php://input"));
    
    $item->id = $data->id;
    
    if($item->supprimeProduit()){
        echo json_encode("Produit supprimer avec succès !");
    } else{
        echo json_encode("Erreur, produit non supprimer !");
    }
?>