<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../Config/database.php';
    include_once '../Class/Produit.php';

    $database = new Database();
    $db = $database->getConnection();

    $item = new Produit($db);

    $item->id = isset($_GET['id']) ? $_GET['id'] : die();
  
    $item->getProduitSeul(); 

    if($item->marque != null){
        // creation du tableau
        $emp_arr = array(
            "produit_id" =>  $item->id,
            "marque" => $item->marque,
            "model" => $item->model,
            "prix" => $item->prix,
        );
      
        http_response_code(200);
        echo json_encode($emp_arr);
    }
      
    else{
        http_response_code(404);
        echo json_encode("Erreur de recupération du produit");
    }
?>