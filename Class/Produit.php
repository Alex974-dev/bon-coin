<?php

class Produit{

// Connection
private $conn;

// Table
private $db_table = "produit";

// Colonnes
public $id;
public $marque;
public $model;
public $prix;

// Db connection
public function __construct($db){
    $this->conn = $db;
}

// Function pour selectionner tout les produits

public function getProduitAll(){
    $sqlQuery = "SELECT * FROM " . $this->db_table . "";
    $stmt = $this->conn->prepare($sqlQuery);
    $stmt->execute();
    return $stmt;
}

// Fonction pour ajouter un produit

public function ajoutProduit(){
    $sqlQuery = "INSERT INTO ". $this->db_table ." 
    
    SET 
    marque = :marque,
    model = :model,
    prix = :prix";
                

    $stmt = $this->conn->prepare($sqlQuery);

    // sanitize
    $this->marque=htmlspecialchars(strip_tags($this->marque));
    $this->model=htmlspecialchars(strip_tags($this->model));
    $this->prix=htmlspecialchars(strip_tags($this->prix));

    // bind data
    $stmt->bindParam(":marque", $this->marque);
    $stmt->bindParam(":model", $this->model);
    $stmt->bindParam(":prix", $this->prix);

    if($stmt->execute()){
       return true;
    }
    return false;
}

// Function pour recup un seul produit

public function getProduitSeul(){
    $sqlQuery = "SELECT
                produit_id, 
                marque, 
                model, 
                prix
              FROM
                ". $this->db_table ."
            WHERE 
               produit_id = ?";

    $stmt = $this->conn->prepare($sqlQuery);

    $stmt->bindParam(1, $this->id);

    $stmt->execute();

    $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
    
    $this->marque = $dataRow['marque'];
    $this->model = $dataRow['model'];
    $this->prix = $dataRow['prix'];
}        

// UPDATE
public function updateProduit(){
    $sqlQuery = "UPDATE
                ". $this->db_table ."
            SET
                marque = :marque, 
                model = :model, 
                prix = :prix
            WHERE 
                produit_id = :id";

    $stmt = $this->conn->prepare($sqlQuery);

    $this->marque=htmlspecialchars(strip_tags($this->marque));
    $this->model=htmlspecialchars(strip_tags($this->model));
    $this->prix=htmlspecialchars(strip_tags($this->prix));
    $this->produit_id=htmlspecialchars(strip_tags($this->id));

    // bind data
    $stmt->bindParam(":marque", $this->marque);
    $stmt->bindParam(":model", $this->model);
    $stmt->bindParam(":prix", $this->prix);
    $stmt->bindParam(":id", $this->id);

    if($stmt->execute()){
       return true;
    }
    return false;
}

// Function pour la methode DELETE
public function supprimeProduit(){
    $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE produit_id = ?";
    $stmt = $this->conn->prepare($sqlQuery);

    $this->produit_id=htmlspecialchars(strip_tags($this->id));

    $stmt->bindParam(1, $this->id);

    if($stmt->execute()){
        return true;
    }
    return false;
}

}